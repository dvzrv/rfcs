Moderator role for the AUR
==========================

- Date proposed: 2023-06-27
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/19

Summary
-------

Create a dedicated role for the moderation of the AUR that supports and eventually replaces the Package Maintainers as the current moderators.

Motivation
----------

Currently, Arch Linux has 64 `Package Maintainers`. Historically known as `Trusted Users` (TU), they had two main responsibilities: Moderation of the AUR and moving popular AUR packages to the official Arch Linux repositories. The combination of those two responsibilities has not worked out since quite some time, as most package maintainers simply do not really intend to moderate the platform. On the other hand potential moderators have a hard time applying as package maintainers just for the sake of moderating the AUR. This RFC proposes to split the old TU role into `Package Maintainers` and `AUR Moderators` to have a clear separation of concern and allow for an easier way to recruit `AUR Moderators`.

Specification
-------------

Similar to forum moderators, `AUR Moderators` review requests on the AUR and accept or close them.
They must not handle their own requests.
Furthermore, they are allowed to suspend accounts that violate the AUR contribution guidelines.
With a successful application, they are onboarded as Arch Linux staff.

At the moment AUR web defines the following user types

* `Normal user`
* `Package Maintainer`
* `Developer`
* `Package Maintainer & Developer`

After the implementation of this RFC the user types will be changed to

* `Normal User`
* `AUR Moderator`
* `Package Maintainer`
* `Package Maintainer & AUR Moderator`

As discussed above, an `AUR Moderator` has the rights of a `Package Maintainer`
on the AUR in the old role.
With the new roles, a `Package Maintainer` loses their ability to moderate
the AUR by default. However, at any time, a `Package Maintainer` can gain moderation
rights on the AUR, promoting them to `Package Maintainer & AUR Moderator`.

`AUR Moderators` are recruited from the regular contributors of the AUR.
To be promoted, users must secure the support of one `AUR Moderator`.
They can seek for sponsorship support by contacting staff members directly.
Once a sponsor is found, the nominee writes an application email signed with either the SSH or GPG key of their AUR account to the aur-general mailing list.
After the sponsor confirmed their sponsorship, a discussion period of seven days starts.
Subsequently, the `AUR Moderators` cast their vote.
For a successful application, the nominee has to gain a simple majority
of all votes.
The responsibility of the sponsor is to guide the rookie through the duties of an `AUR Moderator` while regularly checking in on their work during their first month.
In general, the responsibility of all `AUR Moderators` and the applicant is to be up to date on the latest AUR submission guidelines [1] and Arch package guidelines [2].
Detailed guidelines for the new role of `AUR Moderators` will be worked out by the initial
group of `AUR Moderators` based on this RFC and related existing documents.

Drawbacks
---------

Packages in the AUR are not officially supported by Arch Linux. Adding a special role to handle AUR requests may lead to the impression that the AUR is an official package source. However, AUR Moderators will only watch over the AUR submission guidelines [1] and Arch packaging guidelines [2].

Unresolved Questions
--------------------

None

Alternatives Considered
-----------------------

Package Maintainers should take care of the AUR more regularly. One possibility is to name a group of up to five Package Maintainers who will focus on AUR requests for one week. A list with names is published as a markdown file every month.

[1] https://wiki.archlinux.org/title/AUR_submission_guidelines
[2] https://wiki.archlinux.org/title/Arch_package_guidelines
