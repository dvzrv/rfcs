---
type: docs
---

A Request for Comment
([RFC](https://en.wikipedia.org/wiki/Request_for_Comments)) is a way for Arch
Linux contributors to propose, design and discuss new features and changes in
project direction in a focused environment.

RFCs start as merge requests in: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests

Once accepted, they can be found in: https://gitlab.archlinux.org/archlinux/rfcs/-/tree/master/rfcs
